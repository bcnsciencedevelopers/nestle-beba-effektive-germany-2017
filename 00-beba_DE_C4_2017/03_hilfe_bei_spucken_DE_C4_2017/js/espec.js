/* Author: Ruth CJ */

jQuery(document).ready(function () {

    /* MENU GENERATION */
    var topMenuActives = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    topMenu.init(topMenuActives);
    var sideMenuActives = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    sideMenu.init(sideMenuActives);
    var sideMenuActives3 = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    sideMenu3.init(sideMenuActives3);
    //Inicialitzar l'edetailing 
    edetailing.init(estructura);

});

