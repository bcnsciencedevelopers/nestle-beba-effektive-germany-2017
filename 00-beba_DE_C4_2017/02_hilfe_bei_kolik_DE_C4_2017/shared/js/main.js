jQuery(document).ready(function () {

    $(".global-regular").append('<div class="side-menu-toggle ic-open-menu"></div>');
    $(".side-menu-toggle").on("touchend", function () {
        $(this).toggleClass("active");
        $("#side-menu .nivell-1").toggleClass("active");
    });

    /* POPUP REF GENERATION */
    var popupRef = "";
    popupRef += "<div class='ic-ref btn-ref js-mostrar-popup btn btn-react' data-popup='#ref'></div>";
    popupRef += "<div id='ref' class='popup popup-fade' data-animacio='fade' data-pixels='0'>";
    popupRef += "    <div class='content'></div>";
    popupRef += "    <div class='js-tancar-popup btn btn-close btn-react'></div>";
    popupRef += "</div>";
    $(".global-regular").append(popupRef);

    /* POPUP INSTRUCTIONS GENERATION */
    var popupIns = "";
    popupIns += "<div id='instructions' class='popup popup-fade' data-animacio='fade' data-pixels='0'>";
    popupIns += "    <div class='content'></div>";
    popupIns += "    <div class='js-tancar-popup btn btn-close btn-react'></div>";
    popupIns += "</div>";
    $(".global-regular").append(popupIns);

    /* POPUP DISCLAIMER GENERATION */
    var popupDisc = "";
    popupDisc += "<div id='disclaimer' class='popup p2 popup-fade' data-animacio='fade' data-pixels='0'>";
    popupDisc += "    <div class='content'></div>";
    popupDisc += "    <div class='js-tancar-popup btn btn-close btn-react'></div>";
    popupDisc += "</div>";
    $(".global-regular").append(popupDisc);

    /* POPUP BEBA GENERATION */
    var popupBeba = "";
    popupBeba += "<div id='popup-beba' class='popup p2 popup-fade' data-animacio='fade' data-pixels='0'>";
    popupBeba += "    <div class='content'></div>";
    popupBeba += "    <div class='js-tancar-popup btn btn-close btn-react'></div>";
    popupBeba += "</div>";
    $(".global-regular").append(popupBeba);


    $("#top-menu").on("touchend", ".btn-draw", function () {
        $(this).toggleClass("active");
        $("#drawToolSketchpad").fadeToggle(100);
        if ($(".btn-draw").hasClass("active")) {
            overlay.tool("clear");
        }
    });

//menu de abajo 
    $("#top-menu").after("<div class='btn-side'></div>");
    $(".btn-side").on("touchend", function () {
        $('.btn-side').addClass("active");
        $("#side-menu .nivell-1 ").animate({
            right: 0
        }, 100);
        $("#side-menu2 .nivell-1 ").animate({
            right: 0
        }, 100);


        $(".btn-ref ").animate({
            width: 500
        }, 300);
    });
});


