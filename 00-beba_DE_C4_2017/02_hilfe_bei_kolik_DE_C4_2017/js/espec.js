/* Author: Ruth CJ */

jQuery(document).ready(function () {

    /* MENU GENERATION */
    var topMenuActives = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    topMenu.init(topMenuActives);
    var sideMenuActives = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    sideMenu.init(sideMenuActives);

    var sideMenuActives2 = {"nivell_1": false, "nivell_2": false, "nivell_3": false};
    sideMenu2.init(sideMenuActives2);
    edetailing.init(estructura);

});

