var menuPrincipal;
var estructuraTopMenu;
var estructuraSideMenu;
var estructura;
var sideMenu;
var topMenu;

$(document).ready(function () {
//    var slide = 0;
//    var presentation = 0;
    /**
     * Array que representa l'estructura del material.
     * id de les CLM Presentation
     * + el nom de l'arxiu ZIP de cada Key Message (sense el .zip) de cada presentació
     * */

    // #guia - escriu aquí l'estructura de la teva presentació
    estructura = [];

    estructura[0] = ["beba_DE_C4_2017", [
            "01_effektive_hilfe_bei_gi_Storungen_DE_C4_2017",
            "02_hilfe_bei_kolik_DE_C4_2017",
            "03_hilfe_bei_spucken_DE_C4_2017",
            "04_rome_IV_DE_C4_2017",
            "05_beratung_der_eltern_DE_C4_2017",
            "06_hilfe_bei_sauglingskolik_DE_C4_2017",
            "07_hilfe_bei_spucken_DE_C4_2017",
            "08_rome_IV_DE_C4_2017",
            "09_beratung_der_eltern_DE_C4_2017",
            "10_prozentsatz_sauglingskolik_DE_C4_2017",
            "11_unreife_gi_system_DE_C4_2017",
            "12_effektive_hilfe_bei_kolik_DE_C4_2017",
            "13_verminderte_schreidauer_bes sere_lebensqualitat_DE_C4_2017",
            "14_hilfe_bei_kolik_DE_C4_2017",
            "15_comfort_nahrungen_DE_C4_2017",
            "16_prozentsatz_spucken_DE_C4_2017",
            "17_mogliche_ursachen_spucken_DE_C4_2017",
            "18_effektive_hilfe_spucken_DE_C4_2017",
            "19_beba_ar_klinische_studie_DE_C4_2017",
            "20_beba_ar_kernbotschaften_DE_C4_2017",
            "21_ar_unterschiedlich_zusammengesetzt_DE_C4_2017"

        ]];

    estructura[99] = ["beba_popups_DE_C4_2017", [
            "01_rome_IV_neue_kriterien_DE_C4_2017",
            "02_rome_IV_kriterien_fur_spucken_DE_C4_2017",
            "03_rome_IV_neue_kriterien_kolik_DE_C4_2017",
            "04_rome_IV_kriterien_fur_spucken_DE_C4_2017",
            "05_l_reuteri_bei_kolik_DE_C4_2017",
            "06_l_reuteri_bei_Kolik_2_DE_C4_2017",
            "07_l_reuteri_savino_DE_C4_2017",
            "08_l_reuteri_szajewska_DE_C4_2017",
            "09_molkenprotein_verdaulich_DE_C4_2017",
            "10_molkenprotein_magenentleerung_DE_C4_2017",
            "11_laktosegehalt_vermindert_schreidauer_DE_C4_2017",
            "12_effektive_hilfe_spucken_DE_C4_2017",
            "13_molkenprotein_reguliert_magenentleerung_DE_C4_2017",
            "14_l_reuteri_vermindert_spucken_DE_C4_2017",
            "15_l_reuteri_indrio_DE_C4_2017",
            "16_starke_vermindert_spucken_DE_C4_2017"
        ]];


    estructuraTopMenu = [
        {
            "text": false,
            "ic": "ic-top-menu1",
            "classes": "btn-disclaimer",
            "popup": "#disclaimer",
            "goto": false,
            "children": false
        },
        {
            "text": false,
            "ic": "ic-top-menu4",
            "goto": {
                "slide": "1",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": false,
            "ic": "ic-top-menu2",
            "classes": "btn-draw",
            "goto": false,
            "children": false
        },
        {
            "text": false,
            "ic": "ic-top-menu3",
            "goto": {
                "slide": "0",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];


    estructuraSideMenu = [
        {
            "text": "<span class='menu-text'>KURZVERSION</span>",
            "ic": "icon-side-menu1",
            "goto": {
                "slide": "3",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>ROME IV</span>",
            "ic": "icon-side-menu2",
            "goto": {
                "slide": "7",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>S&#196;UGLINGSKOLIK,<br/>SCHREIBABYS</span>",

            "ic": "icon-side-menu3",
            "goto": {
                "slide": "9",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>SPUCKEN,<br/>REGURGITATION</span>",

            "ic": "icon-side-menu4",
            "goto": {
                "slide": "15",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];

    estructuraSideMenu2 = [
        {
            "text": "<span class='menu-text'>Prävalenz</span>",
            "ic": "icon-side-menu5",
            "goto": {
                "slide": "9",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>Ursachen</span>",
            "ic": "icon-side-menu3",
            "goto": {
                "slide": "10",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>Wirkmechanismen</span>",
            "ic": "icon-side-menu6",
            "goto": {
                "slide": "11",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>Nestlé BEBA COMFORT</span>",
            "ic": "icon-side-menu7",
            "goto": {
                "slide": "13",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];

    estructuraSideMenu3 = [
        {
            "text": "<span class='menu-text'>Prävalenz</span>",
            "ic": "icon-side-menu5",
            "goto": {
                "slide": "15",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>Ursachen</span>",
            "ic": "icon-side-menu4",
            "goto": {
                "slide": "16",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>Wirkmechanismen</span>",
            "ic": "icon-side-menu6",
            "goto": {
                "slide": "17",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='menu-text'>BEBA AR</span>",
            "ic": "icon-side-menu7",
            "goto": {
                "slide": "18",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];

    // 2n paràmetre: id de l'element on s'inclourà el menú
    topMenu = new Menu(estructuraTopMenu, 'top-menu');
    sideMenu = new Menu(estructuraSideMenu, 'side-menu');
    sideMenu2 = new Menu(estructuraSideMenu2, 'side-menu2');
    sideMenu3 = new Menu(estructuraSideMenu3, 'side-menu3');

    // #guia - IMPORTANT! Comentar aquesta línia al pujar al Salesforce
    // Habilitar el mode Test durant el desenvolupament i per testejar i debugar funcions de veeva! 
//    edetailing.enableTestMode();

    // Opcional: modificar valors per defecte. Cal fer-ho abans d'inicialitzar l'edetailing.
    edetailing.changeOverlayDefault('opacity', 0.3);
    edetailing.changePopupDefault('temps', 300);
//    edetailing.changeTouchEventDefault('touchstart');

    // 2n paràmetre: id de l'element on s'inclourà el codi




});



