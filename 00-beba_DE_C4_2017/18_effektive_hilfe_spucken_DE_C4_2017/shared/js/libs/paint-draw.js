﻿
        var overlay = {
            drawToolSketchpad: null,
            canvas: null,
            context: null,
            drawTool: null,
            pencil: null,
            pencilGreen: null,
            pencilDani: null,
            brush: null,
            clear: null,
            close: null,
            curSize: 3,
            curColor: "black",
            init: function () {

                this.setElements();

                this.canvas = document.getElementById('sketchpad');
                this.context = this.canvas.getContext('2d');

                // attach the touchstart, touchmove, touchend event listeners.
                this.canvas.addEventListener('touchstart', overlay.draw, false);
                this.canvas.addEventListener('touchmove', overlay.draw, false);
                this.canvas.addEventListener('touchend', overlay.draw, false);
                // this.drawTool = document.getElementById('drawTool');
                this.black = this.drawTool.getElementsByClassName('black')[0];
                this.red = this.drawTool.getElementsByClassName('red')[0];
                this.yellow = this.drawTool.getElementsByClassName('yellow')[0];
                this.blue = this.drawTool.getElementsByClassName('blue')[0];
                this.green = this.drawTool.getElementsByClassName('green')[0];
                this.xsmall = this.drawTool.getElementsByClassName('xsmall')[0];
                this.small = this.drawTool.getElementsByClassName('small')[0];
                this.medium = this.drawTool.getElementsByClassName('medium')[0];
                this.large = this.drawTool.getElementsByClassName('large')[0];
                this.clear = this.drawTool.getElementsByClassName('clear')[0];
                this.drawTool.addEventListener('click', function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('open');
                }, false);
                this.black.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('black');

                }, false);
                this.red.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('red');

                }, false);
                this.yellow.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('yellow');

                }, false);
                this.blue.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('blue');

                }, false);
                this.green.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('green');

                }, false);
                this.xsmall.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('xsmall');

                }, false);
                this.small.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('small');

                }, false);
                this.medium.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('medium');

                }, false);
                this.large.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('large');

                }, false);
                this.clear.addEventListener('touchstart', function (event) {

                    event.stopPropagation();
                    event.preventDefault();
                    overlay.tool('clear');

                }, false);
                document.body.addEventListener('touchmove', function (event) {
                    event.preventDefault();
                }, false);
            },
            setElements: function () {
                this.drawToolSketchpad = document.createElement("div");
                this.drawTool = document.createElement("div");
                this.drawToolSketchpad.setAttribute('id', 'drawToolSketchpad');
                this.drawTool.setAttribute('id', 'drawTool');
                document.body.appendChild(this.drawToolSketchpad);
                document.body.appendChild(this.drawTool);
                this.drawToolSketchpad.innerHTML = '<canvas id="sketchpad" width="2048" height="1536"></canvas>';
                this.drawTool.innerHTML =
                        '<div class="black"></div>'
                        + '<div class="red"></div>'
                        + '<div class="yellow"></div>'
                        + '<div class="blue"></div>'
                        + '<div class="green"></div>'
                        + '<div class="xsmall tama"><div class="p-xsmall"></div></div>'
                        + '<div class="small tama"><div class="p-small"></div></div>'
                        + '<div class="medium tama"><div class="p-medium"></div></div>'
                        + '<div class="large tama"><div class="p-large"></div></div>'
                        + '<div class="clear"></div>';

            },
            // Function which tracks touch movements
            drawer: {
                isDrawing: false,
                touchstart: function (coors) {
                    overlay.context.beginPath();
                    overlay.context.lineJoin = "round";
                    overlay.context.moveTo(coors.x, coors.y);
                    this.isDrawing = true;
                },
                touchmove: function (coors) {
                    if (this.isDrawing) {
                        overlay.context.lineWidth = overlay.curSize;
                        overlay.context.strokeStyle = overlay.curColor;
                        overlay.context.lineTo(coors.x, coors.y);
                        overlay.context.stroke();
                        //overlay.context.style.opacity = "0.7";
                    }
                },
                touchend: function (coors) {
                    if (this.isDrawing) {
                        this.touchmove(coors);
                        this.isDrawing = false;
                    }
                }
            },
            // Function to pass touch events and coordinates to drawer
            draw: function (event) {
                // get the touch coordinates
                if (event.targetTouches[0]) {
                    var coors = {
                        x: event.targetTouches[0].pageX,
                        y: event.targetTouches[0].pageY
                    };
                    // pass the coordinates to the appropriate handler
                    overlay.drawer[event.type](coors);
                }
            },
            tool: function (toDo) {
                switch (toDo) {
                    case "black":
                        this.curColor = "black";
                        $("#drawTool").children().children().css({"background-color": "black"});
                        break;
                    case "red":
                        this.curColor = "red";
                        $("#drawTool").children().children().css({"background-color": "red"});
                        break;
                    case "yellow":
                        this.curColor = "yellow";
                        $("#drawTool").children().children().css({"background-color": "yellow"});
                        break;
                    case "blue":
                        this.curColor = "blue";
                        $("#drawTool").children().children().css({"background-color": "blue"});
                        break;
                    case "green":
                        this.curColor = "green";
                        $("#drawTool").children().children().css({"background-color": "green"});
                        break;
                    case "xsmall":
                        this.curSize = 2;
                        $(".tama").css({"border-color": "black"});
                        $(".xsmall").css({"border-color": "#e9501d"});
                        break;
                    case "small":
                        this.curSize = 4;
                        $(".tama").css({"border-color": "black"});
                        $(".small").css({"border-color": "#e9501d"});
                        break;
                    case "medium":
                        this.curSize = 8;
                        $(".tama").css({"border-color": "black"});
                        $(".medium").css({"border-color": "#e9501d"});
                        break;
                    case "large":
                        this.curSize = 16;
                        $(".tama").css({"border-color": "black"});
                        $(".large").css({"border-color": "#e9501d"});
                        break;
                    case "clear":
                        this.context.clearRect(0, 0, 2048, 1536);
                        break;
                }
            }
        };

//document.addEventListener('Load', function(e) {
overlay.init();
//});
